# Create Custom Docker Image
FROM tomcat:latest

# Maintainer
MAINTAINER "Bhargav_DevOps - Bhargav-siripurapu" 

# copy war file on to container 
COPY app/target/login-release.war /usr/local/tomcat/webapps/login.war
EXPOSE 8080